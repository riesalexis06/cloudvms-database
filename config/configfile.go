package config

import (
	"strings"

	"github.com/spf13/viper"
)

type Configurations struct {
	Database Database
}

type Database struct {
	Host     string
	Port     string
	Database string
	User     string
	Password string
}

var DatabaseConfig = &Database{}

func LoadConfigs() (err error) {
	v := viper.New()

	v.SetEnvPrefix("cvms")
	v.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	v.AutomaticEnv()

	if ConfigPath != "" {
		v.SetConfigFile(ConfigPath)
	} else {
		v.SetConfigName("config")
		v.SetConfigType("yaml")
		v.AddConfigPath("/etc/cloudvms/config.yaml")
		v.AddConfigPath("$HOME/.cloudvmsconfig")
		v.AddConfigPath("./")
	}

	if err = v.ReadInConfig(); err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); ok {
			err = nil
		} else {
			return
		}
	}

	config := Configurations{}
	err = v.Unmarshal(&config)

	DatabaseConfig = &config.Database
	return
}
