package config

import "github.com/spf13/pflag"

var ConfigPath string

func LoadOptions() {
	defer pflag.Parse()
	pflag.StringVar(&ConfigPath, "config-path", "", "path to configuration file")
}
