# CloudVMS Database API

This API serves as a database for the various microservices of the CloudVMS platform.

CloudVMS is a cloud-based Video Management System, this service allows you to manage the configuration of surveillance cameras and outsource video recordings.

## Config

The configuration file must be located on one of the following paths:

- ./config.yaml
- /etc/cloudvms/config.yaml
- $HOME/.cloudvmsconfig

Here is an example of a configuration file:

```yaml
database:
  driver: postgresql
  host: localhost
  port: 5432
  database: cloudvms
  user: toto
  password: password
```

If you do not specify a configuration file, the program will use an SQLite database driver by default.

## Run

To run this API for tests, just run this command:

```sh
go run .
```


## Todo :

Here is what remains to be done:

- Generate Swagger RESTful API documentation
- Add a prometheus metrics exporter
- Unit tests and integration tests
- Code quality analysis (QA scan)
- Automated deployment in a staging environment
- KPI extraction, performance tests
- Automated deployment in a production environment