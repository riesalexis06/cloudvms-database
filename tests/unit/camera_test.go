package modeltests

import (
	"cloudvms-database/api/models"
	"testing"

	"gopkg.in/DATA-DOG/go-sqlmock.v1"
	"gopkg.in/go-playground/assert.v1"
)

func TestValidateCamera(t *testing.T) {
	camera := models.Camera{
		Name:         "testCamera",
		RtspAddress:  "fake_address",
		RtspUsername: "fake_username",
		RtspPassword: "fake_password",
	}
	err := camera.Validate()
	if err != nil {
		t.Errorf("this is the error validating the collector: %v\n", err)
		return
	}
	assert.Equal(t, err, nil)
}

func TestValidateCameraWithoutData(t *testing.T) {
	// expect error if there is no input data
	camera := models.Camera{}
	err := camera.Validate()
	assert.NotEqual(t, err, nil)
	assert.Equal(t, camera, models.Camera{})
}

func TestCreateCamera(t *testing.T) {
	_, _, camera, _, _, _ := fakeSeedOneCompanyAndOneCollectorAndOneCamera()

	mock.ExpectQuery(`INSERT INTO "cameras" .* VALUES .* RETURNING .*`).
		WithArgs(
			camera.Name, camera.CollectorId, camera.Type, camera.RtspAddress,
			camera.RtspUsername, camera.RtspPassword, sqlmock.AnyArg(),
			sqlmock.AnyArg(), camera.CameraId,
		).
		WillReturnRows(sqlmock.NewRows([]string{"created_at", "updated_at", "camera_id"}).
			AddRow(camera.CreatedAt, camera.UpdatedAt, camera.CameraId))

	addedCamera, err := camera.CreateCamera(server.DB)
	if err != nil {
		t.Errorf("this is the error creating the collector: %v\n", err)
		return
	}
	assert.Equal(t, addedCamera.Name, camera.Name)
	assert.Equal(t, addedCamera.CollectorId, camera.CollectorId)
}

func TestFindCameraById(t *testing.T) {
	_, _, camera, _, _, cameraRows := fakeSeedOneCompanyAndOneCollectorAndOneCamera()

	mock.ExpectQuery(`SELECT \* FROM "cameras" WHERE .* LIMIT 1`).
		WithArgs(camera.CameraId).
		WillReturnRows(cameraRows)

	foundCamera, err := cameraInstance.FindCameraByID(server.DB, camera.CameraId)
	if err != nil {
		t.Errorf("this is the error finding the collector: %v\n", err)
		return
	}

	assert.Equal(t, foundCamera.CameraId, camera.CameraId)
	assert.Equal(t, foundCamera.Name, camera.Name)
	assert.Equal(t, foundCamera.CollectorId, camera.CollectorId)
}

func TestFindCameraByIdDontExists(t *testing.T) {
	nonexistant_camera_id := uint64(999)

	emptyRows := sqlmock.NewRows(companyColumns)
	mock.ExpectQuery(`SELECT \* FROM "cameras" WHERE .* LIMIT 1`).
		WithArgs(nonexistant_camera_id, sqlmock.AnyArg()).
		WillReturnRows(emptyRows)

	foundCamera, err := cameraInstance.FindCameraByID(server.DB, nonexistant_camera_id)
	assert.NotEqual(t, err, nil)
	assert.Equal(t, foundCamera, nil)
}
