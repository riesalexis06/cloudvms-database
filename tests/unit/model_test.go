package modeltests

import (
	"cloudvms-database/api/controllers"
	"cloudvms-database/api/models"
	"cloudvms-database/config"
	"database/sql"
	"fmt"
	"log"
	"os"
	"testing"
	"time"

	"gopkg.in/DATA-DOG/go-sqlmock.v1"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

var mock sqlmock.Sqlmock
var server = controllers.Server{}

var companyInstance = models.Company{}
var collectorInstance = models.Collector{}
var cameraInstance = models.Camera{}

var companyColumns = []string{"company_id", "name", "created_at", "updated_at"}
var collectorColumns = []string{"collector_id", "name", "company_id", "created_at", "updated_at"}
var cameraColumns = []string{
	"camera_id", "name", "collector_id", "type",
	"rtsp_address", "rtsp_username", "rtsp_password", "created_at", "updated_at",
}

func TestMain(m *testing.M) {
	var (
		sqlDB *sql.DB
		err   error
	)
	sqlDB, mock, err = sqlmock.New()
	if err != nil {
		log.Fatal(err)
	}
	if sqlDB == nil {
		log.Fatal("mock sqlDB is null")
	}
	if mock == nil {
		log.Fatal("sqlmock is null")
	}

	config.LoadConfigs()
	err = InitializeFakeDatabase(config.DatabaseConfig, sqlDB)
	if err != nil {
		log.Fatal(err)
	}
	os.Exit(m.Run())
}

func InitializeFakeDatabase(cfg *config.Database, conn *sql.DB) (err error) {
	config := &gorm.Config{
		SkipDefaultTransaction: true,
		Logger:                 logger.Default.LogMode(logger.Silent),
	}
	server.DB, err = gorm.Open(postgres.New(postgres.Config{
		DSN: fmt.Sprintf("host=%s port=%s user=%s dbname=%s sslmode=disable password=%s",
			cfg.Host, cfg.Port, cfg.User, cfg.Database, cfg.Password),
		Conn: conn,
	}), config)
	if err != nil {
		return fmt.Errorf("cannot connect to postgres database\n error:%s", err)
	}
	return nil
}

func fakeSeedOneCompany() (company *models.Company, companyRows *sqlmock.Rows) {
	company = &models.Company{
		Name:      "testCompany",
		CompanyId: 1,
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	}
	rows := sqlmock.
		NewRows(companyColumns).
		AddRow(company.CompanyId, company.Name, company.CreatedAt, company.UpdatedAt)
	return company, rows
}

func fakeSeedManyCompany() (companies *[]models.Company, companiesRows *sqlmock.Rows) {
	companies = &[]models.Company{
		{
			Name:      "testCompany1",
			CompanyId: 1,
			CreatedAt: time.Now(),
			UpdatedAt: time.Now(),
		},
		{
			Name:      "testCompany2",
			CompanyId: 2,
			CreatedAt: time.Now(),
			UpdatedAt: time.Now(),
		},
	}
	rows := sqlmock.NewRows(companyColumns)
	for _, company := range *companies {
		rows.AddRow(company.CompanyId, company.Name,
			company.CreatedAt, company.UpdatedAt)
	}
	return companies, rows
}

func fakeSeedOneCompanyAndOneCollector() (
	company *models.Company, collector *models.Collector,
	companyRows *sqlmock.Rows, collectorRows *sqlmock.Rows,
) {
	company, companyRows = fakeSeedOneCompany()
	collector = &models.Collector{
		Name:        "Collector1",
		CollectorId: 1,
		CompanyId:   company.CompanyId,
		CreatedAt:   time.Now(),
		UpdatedAt:   time.Now(),
	}
	collectorRows = sqlmock.
		NewRows(collectorColumns).
		AddRow(collector.CollectorId, collector.Name, collector.CompanyId,
			collector.CreatedAt, collector.UpdatedAt)

	return company, collector, companyRows, collectorRows
}

func fakeSeedManyCompanyAndManyCollector() (
	companies *[]models.Company, collectors *[]models.Collector,
	companyRows *sqlmock.Rows, collectorRows *sqlmock.Rows,
) {
	companies, companyRows = fakeSeedManyCompany()

	var addedCollectors []models.Collector
	collectorRows = sqlmock.NewRows(companyColumns)
	collectorsTemplate := []models.Collector{
		{
			Name: "testCollector1",
		},
		{
			Name: "testCollector2",
		},
	}

	for _, company := range *companies {
		for i, collector := range collectorsTemplate {
			companyCollector := models.Collector{
				CollectorId: uint64(i + 1),
				Name:        collector.Name,
				CompanyId:   company.CompanyId,
				CreatedAt:   time.Now(),
				UpdatedAt:   time.Now(),
			}
			addedCollectors = append(addedCollectors, companyCollector)
			collectorRows.AddRow(companyCollector.CollectorId,
				companyCollector.Name, time.Now(), time.Now(),
			)
		}
	}
	return companies, &addedCollectors, companyRows, collectorRows
}

func fakeSeedOneCompanyAndOneCollectorAndOneCamera() (
	company *models.Company, collector *models.Collector, camera *models.Camera,
	companyRows *sqlmock.Rows, collectorRows *sqlmock.Rows, cameraRows *sqlmock.Rows,
) {
	company, collector, companyRows, collectorRows = fakeSeedOneCompanyAndOneCollector()
	camera = &models.Camera{
		CameraId:     1,
		Name:         "CameraTest",
		Type:         models.CameraTypeSimple,
		CollectorId:  collector.CollectorId,
		RtspAddress:  "localhost",
		RtspUsername: "fake_user",
		RtspPassword: "fake_password",
	}
	cameraRows = sqlmock.
		NewRows(cameraColumns).
		AddRow(camera.CameraId, camera.Name, camera.CollectorId, camera.Type, camera.RtspAddress,
			camera.RtspUsername, camera.RtspPassword, collector.CreatedAt, collector.UpdatedAt)

	return company, collector, camera, companyRows, collectorRows, cameraRows
}

func fakeSeedManyCompanyAndManyCollectorAndManyCameras() (
	companies *[]models.Company, collectors *[]models.Collector, cameras *[]models.Camera,
	companyRows *sqlmock.Rows, collectorRows *sqlmock.Rows, camerasRows *sqlmock.Rows,
) {
	companies, collectors, companyRows, collectorRows = fakeSeedManyCompanyAndManyCollector()

	var addedCameras []models.Camera
	camerasRows = sqlmock.NewRows(companyColumns)
	camerasTemplate := []models.Camera{
		{
			Name:         "CameraTest1",
			Type:         models.CameraTypeSimple,
			RtspAddress:  "localhost",
			RtspUsername: "fake_user1",
			RtspPassword: "fake_password1",
		},
		{
			Name:         "CameraTest2",
			Type:         models.CameraTypeSimple,
			RtspAddress:  "localhost",
			RtspUsername: "fake_user2",
			RtspPassword: "fake_password2",
		},
	}

	for _, collector := range *collectors {
		for i, camera := range camerasTemplate {
			companyCamera := models.Camera{
				CameraId:    uint64(i + 1),
				Name:        camera.Name,
				CollectorId: collector.CollectorId,
				CreatedAt:   time.Now(),
				UpdatedAt:   time.Now(),
			}
			addedCameras = append(addedCameras, companyCamera)
			camerasRows.AddRow(
				companyCamera.CameraId, companyCamera.Name, time.Now(), time.Now(),
			)
		}
	}
	return companies, collectors, &addedCameras, companyRows, collectorRows, camerasRows
}
