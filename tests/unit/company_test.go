package modeltests

import (
	"cloudvms-database/api/models"
	"regexp"
	"testing"
	"time"

	"gopkg.in/DATA-DOG/go-sqlmock.v1"
	"gopkg.in/go-playground/assert.v1"
)

func TestValidateCompany(t *testing.T) {
	company := models.Company{
		Name: "testCompany",
	}
	err := company.Validate()
	if err != nil {
		t.Errorf("this is the error validating the company: %v\n", err)
		return
	}
}

func TestValidateCompanyWithoutData(t *testing.T) {
	// expect error if there is no input data
	company := models.Company{}
	err := company.Validate()
	assert.NotEqual(t, err, nil)
	assert.Equal(t, company, models.Company{})
}

func TestFindAllCompanies(t *testing.T) {
	companies, rows := fakeSeedManyCompany()

	const query = `SELECT * FROM "companies" LIMIT 100`
	mock.ExpectQuery(regexp.QuoteMeta(query)).WillReturnRows(rows)

	dbcompanies, err := companyInstance.FindAllcompanies(server.DB)
	if err != nil {
		t.Errorf("this is the error getting the companies: %v\n", err)
		return
	}
	assert.Equal(t, len(*dbcompanies), len(*companies))
}

func TestFindAllCompaniesEmpty(t *testing.T) {
	rows := sqlmock.NewRows(companyColumns)

	const query = `SELECT * FROM "companies" LIMIT 100`
	mock.ExpectQuery(regexp.QuoteMeta(query)).WillReturnRows(rows)

	dbcompanies, err := companyInstance.FindAllcompanies(server.DB)
	if err != nil {
		t.Errorf("this is the error getting the companies: %v\n", err)
		return
	}
	assert.Equal(t, len(*dbcompanies), 0)
}

func TestCreateCompany(t *testing.T) {
	newCompany := models.Company{
		Name: "testCompany",
	}

	mock.ExpectQuery(`INSERT INTO "companies" .* VALUES .* RETURNING .*`).
		WithArgs(newCompany.Name, sqlmock.AnyArg(), sqlmock.AnyArg()).
		WillReturnRows(sqlmock.NewRows([]string{"created_at", "updated_at", "company_id"}).
			AddRow(time.Now(), time.Now(), uint64(1)))

	savedCompany, err := newCompany.CreateCompany(server.DB)
	if err != nil {
		t.Errorf("this is the error creating the company: %v\n", err)
		return
	}
	assert.Equal(t, savedCompany.CompanyId, uint64(1))
	assert.Equal(t, savedCompany.Name, newCompany.Name)
}

func TestFindCompanyById(t *testing.T) {
	company, rows := fakeSeedOneCompany()

	const query = `SELECT * FROM "companies" WHERE company_id = $1 LIMIT 1`
	mock.ExpectQuery(regexp.QuoteMeta(query)).
		WithArgs(company.CompanyId).
		WillReturnRows(rows)

	foundCompany, err := companyInstance.FindCompanyByID(server.DB, company.CompanyId)
	if err != nil {
		t.Errorf("this is the error getting one company: %v\n", err)
		return
	}
	assert.Equal(t, foundCompany.CompanyId, company.CompanyId)
	assert.Equal(t, foundCompany.Name, company.Name)
}

func TestFindCompanyByIdDontExists(t *testing.T) {
	nonexistant_company_id := uint64(999)

	emptyRows := sqlmock.NewRows(companyColumns)
	mock.ExpectQuery(`SELECT \* FROM "companies" WHERE .*`).
		WithArgs(nonexistant_company_id, sqlmock.AnyArg()).
		WillReturnRows(emptyRows)

	foundCompany, err := companyInstance.FindCompanyByID(server.DB, nonexistant_company_id)
	assert.NotEqual(t, err, nil)
	assert.Equal(t, foundCompany, nil)
}

func TestUpdateCompany(t *testing.T) {
	company, rows := fakeSeedOneCompany()

	companyUpdate := models.Company{
		CompanyId: 1,
		Name:      "Toto",
	}

	const query1 = `SELECT * FROM "companies" WHERE company_id = $1 LIMIT 1`
	mock.ExpectQuery(regexp.QuoteMeta(query1)).
		WithArgs(company.CompanyId).
		WillReturnRows(rows)

	const query2 = `UPDATE "companies" SET "name"=$1,"updated_at"=$2 WHERE company_id = $3`
	mock.ExpectExec(regexp.QuoteMeta(query2)).
		WithArgs(companyUpdate.Name, sqlmock.AnyArg(), company.CompanyId).
		WillReturnResult(sqlmock.NewResult(1, 1))

	rows = sqlmock.
		NewRows(companyColumns).
		AddRow(company.CompanyId, companyUpdate.Name, company.CreatedAt, time.Now())

	const query3 = `SELECT * FROM "companies" WHERE company_id = $1 AND company_id = $2 AND "companies"."company_id" = $3 LIMIT 1`
	mock.ExpectQuery(regexp.QuoteMeta(query3)).
		WithArgs(companyUpdate.CompanyId, companyUpdate.CompanyId, companyUpdate.CompanyId).
		WillReturnRows(rows)

	updatedCompany, err := companyUpdate.UpdateCompany(server.DB, company.CompanyId)
	if err != nil {
		t.Errorf("this is the error updating the company: %v\n", err)
		return
	}
	assert.Equal(t, updatedCompany.CompanyId, companyUpdate.CompanyId)
	assert.Equal(t, updatedCompany.Name, companyUpdate.Name)
}

func TestUpdateCompanyDontExists(t *testing.T) {
	companyUpdate := models.Company{
		CompanyId: 999,
		Name:      "Toto",
	}
	emptyRows := sqlmock.NewRows(companyColumns)
	mock.ExpectQuery(`SELECT \* FROM "companies" WHERE .*`).
		WithArgs(companyUpdate.CompanyId).
		WillReturnRows(emptyRows)

	updatedCompany, err := companyUpdate.UpdateCompany(server.DB, companyUpdate.CompanyId)
	assert.NotEqual(t, err, nil)
	assert.Equal(t, updatedCompany, models.Company{})
}

func TestCompanyAddCollector(t *testing.T) {
	company, collector, _, _ := fakeSeedOneCompanyAndOneCollector()

	const query1 = `UPDATE "companies" SET "updated_at"=$1 WHERE "company_id" = $2`
	mock.ExpectExec(regexp.QuoteMeta(query1)).
		WithArgs(sqlmock.AnyArg(), company.CompanyId).
		WillReturnResult(sqlmock.NewResult(1, 1))

	mock.ExpectQuery(`INSERT INTO "collectors" .* VALUES .* DO UPDATE SET .*`).
		WillReturnRows(sqlmock.NewRows([]string{"created_at", "updated_at", "collector_id"}).
			AddRow(collector.CreatedAt, collector.UpdatedAt, collector.CollectorId))

	addedCollector, _ := company.AddCollector(server.DB, collector)

	assert.Equal(t, addedCollector.CompanyId, collector.CompanyId)
	assert.Equal(t, addedCollector.Name, addedCollector.Name)
}

func TestDeleteCompany(t *testing.T) {
	company, _ := fakeSeedOneCompany()

	const query = `DELETE FROM "companies" WHERE company_id = $1`
	mock.ExpectExec((regexp.QuoteMeta(query))).
		WithArgs(company.CompanyId).
		WillReturnResult(sqlmock.NewResult(1, 1))

	deletedRows, err := companyInstance.DeleteCompany(server.DB, uint32(company.CompanyId))
	if err != nil {
		t.Errorf("this is the error deleting the company: %v\n", err)
		return
	}
	assert.Equal(t, deletedRows, int64(1))
}

func TestDeleteCompanyDontExists(t *testing.T) {
	company, _ := fakeSeedOneCompany()

	const query = `DELETE FROM "companies" WHERE company_id = $1`
	mock.ExpectExec((regexp.QuoteMeta(query))).
		WithArgs(company.CompanyId).
		WillReturnResult(sqlmock.NewResult(1, 0))

	deletedRows, err := companyInstance.DeleteCompany(server.DB, uint32(company.CompanyId))
	assert.NotEqual(t, err, nil)
	assert.Equal(t, deletedRows, int64(0))
}
