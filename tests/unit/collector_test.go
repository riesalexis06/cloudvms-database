package modeltests

import (
	"cloudvms-database/api/models"
	"regexp"
	"testing"

	"gopkg.in/DATA-DOG/go-sqlmock.v1"
	"gopkg.in/go-playground/assert.v1"
)

func TestValidateCollector(t *testing.T) {
	collector := models.Collector{
		Name: "testCollector",
	}
	err := collector.Validate()
	if err != nil {
		t.Errorf("this is the error validating the collector: %v\n", err)
		return
	}
	assert.Equal(t, err, nil)
}

func TestValidateCollectorWithoutData(t *testing.T) {
	// expect error if there is no input data
	collector := models.Collector{}
	err := collector.Validate()
	assert.NotEqual(t, err, nil)
	assert.Equal(t, collector, models.Collector{})
}

func TestCreateCollector(t *testing.T) {
	_, collector, _, _ := fakeSeedOneCompanyAndOneCollector()

	const query = `INSERT INTO "collectors" .* VALUES .* RETURNING .*`
	mock.ExpectQuery(query).
		WithArgs(collector.Name, collector.CompanyId, sqlmock.AnyArg(), sqlmock.AnyArg(), collector.CollectorId).
		WillReturnRows(sqlmock.NewRows([]string{"created_at", "updated_at", "collector_id"}).
			AddRow(collector.CreatedAt, collector.UpdatedAt, collector.CollectorId))

	addedCollector, err := collector.CreateCollector(server.DB)
	if err != nil {
		t.Errorf("this is the error creating the collector: %v\n", err)
		return
	}
	assert.Equal(t, addedCollector.Name, collector.Name)
	assert.Equal(t, addedCollector.CompanyId, collector.CompanyId)
}

func TestFindCollectorByID(t *testing.T) {
	_, collector, _, collectorRows := fakeSeedOneCompanyAndOneCollector()

	const query = `SELECT * FROM "collectors" WHERE collector_id = $1 LIMIT 1`
	mock.ExpectQuery(regexp.QuoteMeta(query)).
		WithArgs(collector.CollectorId).
		WillReturnRows(collectorRows)

	foundCollector, err := collectorInstance.FindCollectorByID(server.DB, collector.CollectorId)
	if err != nil {
		t.Errorf("this is the error finding the collector: %v\n", err)
		return
	}
	assert.Equal(t, foundCollector.CollectorId, collector.CollectorId)
	assert.Equal(t, foundCollector.Name, collector.Name)
	assert.Equal(t, foundCollector.CompanyId, collector.CompanyId)
}

func TestFindCollectorByIDDontExists(t *testing.T) {
	nonexistant_collector_id := uint64(999)

	emptyRows := sqlmock.NewRows(companyColumns)
	mock.ExpectQuery(`SELECT \* FROM "collectors" WHERE .* LIMIT 1`).
		WillReturnRows(emptyRows)

	foundCollector, err := collectorInstance.FindCollectorByID(server.DB, nonexistant_collector_id)
	assert.NotEqual(t, err, nil)
	assert.Equal(t, foundCollector, nil)
}

func TestCollectorAddCamera(t *testing.T) {
	_, collector, camera, _, _, _ := fakeSeedOneCompanyAndOneCollectorAndOneCamera()

	const query1 = `UPDATE "collectors" SET "updated_at"=$1 WHERE "collector_id" = $2`
	mock.ExpectExec(regexp.QuoteMeta(query1)).
		WithArgs(sqlmock.AnyArg(), collector.CollectorId).
		WillReturnResult(sqlmock.NewResult(1, 1))

	mock.ExpectQuery(`INSERT INTO "cameras" .* VALUES .* RETURNING .*`).
		WithArgs(
			camera.Name, camera.CollectorId, camera.Type, camera.RtspAddress, camera.RtspUsername,
			camera.RtspPassword, camera.CameraId,
		).
		WillReturnRows(sqlmock.NewRows([]string{"created_at", "updated_at", "camera_id"}).
			AddRow(camera.CreatedAt, camera.UpdatedAt, camera.CameraId))

	addedCamera, err := collector.AddCamera(server.DB, camera)
	if err != nil {
		t.Errorf("this is the error adding the camera to collector: %v\n", err)
		return
	}
	assert.Equal(t, addedCamera.CollectorId, camera.CollectorId)
	assert.Equal(t, addedCamera.Name, camera.Name)
	assert.Equal(t, addedCamera.Type, camera.Type)
	assert.Equal(t, addedCamera.RtspAddress, camera.RtspAddress)
	assert.Equal(t, addedCamera.RtspUsername, camera.RtspUsername)
	assert.Equal(t, addedCamera.RtspPassword, camera.RtspPassword)
}

func TestDeleteCollector(t *testing.T) {
	_, collector, _, _ := fakeSeedOneCompanyAndOneCollector()

	const query = `DELETE FROM "collectors" WHERE collector_id = $1`
	mock.ExpectExec((regexp.QuoteMeta(query))).
		WithArgs(collector.CollectorId).
		WillReturnResult(sqlmock.NewResult(1, 1))

	isDeleted, err := collectorInstance.DeleteCollector(server.DB, uint32(collector.CollectorId))
	if err != nil {
		t.Errorf("this is the error deleting the collector: %v\n", err)
		return
	}
	assert.Equal(t, isDeleted, int64(1))
}

func TestDeleteCollectorDontExists(t *testing.T) {
	_, collector, _, _ := fakeSeedOneCompanyAndOneCollector()

	mock.ExpectExec(`DELETE FROM "collectors" WHERE collector_id = \$1`).
		WithArgs(collector.CollectorId).
		WillReturnResult(sqlmock.NewResult(1, 0))

	deletedRows, err := collectorInstance.DeleteCollector(server.DB, uint32(collector.CollectorId))
	assert.NotEqual(t, err, nil)
	assert.Equal(t, deletedRows, int64(0))
}
