package models

import (
	"errors"
	"time"

	"gorm.io/gorm"
)

const (
	CameraTypeSimple = iota
	CameraTypePtz
)

type Camera struct {
	CameraId     uint64    `gorm:"primary_key;auto_increment" json:"id"`
	Name         string    `gorm:"size:255;not null" json:"name"`
	CollectorId  uint64    `gorm:"not null" json:"collector_id"`
	Type         uint8     `gorm:"not null" json:"type"`
	RtspAddress  string    `gorm:"size:255;not null" json:"rtsp_address"`
	RtspUsername string    `gorm:"size:255;not null" json:"rtsp_username"`
	RtspPassword string    `gorm:"size:255;not null" json:"rtsp_password"`
	CreatedAt    time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"created_at"`
	UpdatedAt    time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"updated_at"`
}

type CameraRequest struct {
	Name         string `json:"name"`
	Type         uint8  `json:"type"`
	CollectorId  uint64 `json:"collector_id" binding:"required"`
	RtspAddress  string `json:"rtsp_address"`
	RtspUsername string `json:"rtsp_username"`
	RtspPassword string `json:"rtsp_password"`
}

func (bc *Camera) Validate() error {
	switch {
	case bc.Name == "":
		return errors.New("required camera name")
	case bc.RtspAddress == "":
		return errors.New("required rtsp address")
	case bc.RtspUsername == "":
		return errors.New("required rtsp username")
	case bc.RtspPassword == "":
		return errors.New("required rtsp password")
	case bc.Type != CameraTypeSimple && bc.Type != CameraTypePtz:
		return errors.New("invalid camera type")
	}
	return nil
}

func (bc *Camera) CreateCamera(db *gorm.DB) (*Camera, error) {
	bc.CreatedAt = time.Now()
	bc.UpdatedAt = time.Now()

	err := db.Model(&Camera{}).Create(&bc).Error
	return bc, err
}

func (bc *Camera) FindCameraByID(db *gorm.DB, cid uint64) (*Camera, error) {
	if err := db.Model(&Camera{}).Where("camera_id = ?", cid).Take(&bc).Error; err != nil {
		return nil, err
	}
	return bc, nil
}

func (bc *Camera) UpdateCamera(db *gorm.DB) (*Camera, error) {
	if err := db.Model(&Camera{}).Where("camera_id = ?", bc.CameraId).Updates(Camera{
		Name:         bc.Name,
		RtspAddress:  bc.RtspAddress,
		RtspUsername: bc.RtspUsername,
		RtspPassword: bc.RtspPassword,
		UpdatedAt:    bc.UpdatedAt,
	}).Error; err != nil {
		return nil, err
	}
	return bc, nil
}

func (bc *Camera) DeleteCamera(db *gorm.DB, cid uint64) (int64, error) {
	db = db.Model(&Camera{}).Where("camera_id = ?", cid).Take(&Camera{}).Delete(&Camera{})
	if db.Error != nil {
		if db.Error == gorm.ErrRecordNotFound {
			return 0, errors.New("camera not found")
		}
		return 0, db.Error
	}
	return db.RowsAffected, nil
}
