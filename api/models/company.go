package models

import (
	"errors"
	"fmt"
	"time"

	"gorm.io/gorm"
)

type Company struct {
	CompanyId  uint64      `gorm:"primary_key;auto_increment" json:"id"`
	Name       string      `gorm:"size:255;not null;unique" json:"name"`
	Collectors []Collector `gorm:"foreignKey:CompanyId;constraint:OnUpdate:CASCADE,OnDelete:CASCADE"`
	CreatedAt  time.Time   `gorm:"default:CURRENT_TIMESTAMP" json:"created_at"`
	UpdatedAt  time.Time   `gorm:"default:CURRENT_TIMESTAMP" json:"updated_at"`
}

type CompanyInput struct {
	Name string `json:"name"`
}

func (com *Company) Validate() error {
	switch {
	case com.Name == "":
		return errors.New("required company name")
	}
	return nil
}

func (com *Company) CreateCompany(db *gorm.DB) (*Company, error) {
	com.CreatedAt = time.Now()
	com.UpdatedAt = time.Now()
	err := db.Model(&Company{}).Create(&com).Error
	if err != nil {
		return nil, err
	}
	return com, nil
}

func (com *Company) UpdateCompany(db *gorm.DB, cid uint64) (*Company, error) {
	db = db.Model(&Company{}).Where("company_id = ?", cid).Take(&Company{}).UpdateColumns(
		map[string]interface{}{
			"name":       com.Name,
			"updated_at": time.Now(),
		},
	)
	if db.Error != nil {
		return &Company{}, db.Error
	}
	err := db.Model(&Company{}).Where("company_id = ?", cid).Take(&com).Error
	if err != nil {
		return &Company{}, err
	}
	return com, nil
}

func (bc *Company) FindAllcompanies(db *gorm.DB) (*[]Company, error) {
	companies := []Company{}
	err := db.Model(&Company{}).Limit(100).Find(&companies).Error
	if err != nil {
		return &[]Company{}, err
	}
	return &companies, err
}

func (com *Company) FindCompanyByID(db *gorm.DB, cid uint64) (*Company, error) {
	err := db.Model(&Company{}).Where("company_id = ?", cid).Take(&com).Error
	if err != nil {
		return nil, err
	}
	return com, nil
}

func (com *Company) AddCollector(db *gorm.DB, collector *Collector) (*Collector, error) {
	err := db.Model(&com).Association("Collectors").Append(collector)
	if err != nil {
		return nil, err
	}
	return collector, err
}

func (com *Company) DeleteCompany(db *gorm.DB, cid uint32) (int64, error) {
	db = db.Model(&Company{}).Where("company_id = ?", cid).Delete(&Company{})
	if db.Error != nil {
		return 0, db.Error
	}
	if db.RowsAffected == 0 {
		return 0, fmt.Errorf("the company id %d does not exist", cid)
	}
	return db.RowsAffected, nil
}
