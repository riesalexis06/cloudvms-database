package models

import (
	"errors"
	"fmt"
	"time"

	"gorm.io/gorm"
)

type Collector struct {
	CollectorId uint64    `gorm:"primary_key;auto_increment" json:"id"`
	Name        string    `gorm:"size:255;not null" json:"name"`
	CompanyId   uint64    `gorm:"not null" json:"company_id"`
	Cameras     []Camera  `gorm:"foreignKey:CollectorId"`
	CreatedAt   time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"created_at"`
	UpdatedAt   time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"updated_at"`
}

type CollectorInput struct {
	Name      string `json:"name"`
	CompanyId uint64 `json:"company_id"`
}

func (com *Collector) Validate() error {
	switch {
	case com.Name == "":
		return errors.New("required collector name")
	}
	return nil
}

func (col *Collector) CreateCollector(db *gorm.DB) (*Collector, error) {
	col.CreatedAt = time.Now()
	col.UpdatedAt = time.Now()
	err := db.Model(&Collector{}).Create(&col).Error
	if err != nil {
		return nil, err
	}
	return col, err
}

func (col *Collector) FindCollectorByID(db *gorm.DB, cid uint64) (*Collector, error) {
	err := db.Model(&Collector{}).Where("collector_id = ?", cid).Take(&col).Error
	if err != nil {
		return nil, err
	}
	return col, err
}

func (col *Collector) AddCamera(db *gorm.DB, camera *Camera) (*Camera, error) {
	err := db.Model(&col).Association("Cameras").Append(camera)
	if err != nil {
		return nil, err
	}
	return camera, err
}

func (com *Collector) DeleteCollector(db *gorm.DB, cid uint32) (int64, error) {
	db = db.Model(&Collector{}).Where("collector_id = ?", cid).Delete(&Collector{})
	if db.Error != nil {
		return 0, db.Error
	}
	if db.RowsAffected == 0 {
		return 0, fmt.Errorf("the company id %d does not exist", cid)
	}
	return db.RowsAffected, nil
}
