package controllers

import (
	"cloudvms-database/api/models"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
)

func (server *Server) createCameraHandler(c *gin.Context) {
	//parse inputs
	var cameraInput models.CameraRequest
	if err := c.ShouldBindJSON(&cameraInput); err != nil {
		responseError(c, http.StatusBadRequest, err)
		return
	}

	//check camera collector exists
	var collector models.Collector
	collectorFound, err := collector.FindCollectorByID(server.DB, cameraInput.CollectorId)
	if err != nil {
		responseError(c, http.StatusBadRequest, fmt.Errorf("error on retreiving camera collector : %s", err))
		return
	}

	//build camera object from inputs
	camera := &models.Camera{
		Name:         cameraInput.Name,
		Type:         cameraInput.Type,
		RtspAddress:  cameraInput.RtspAddress,
		RtspUsername: cameraInput.RtspUsername,
		RtspPassword: cameraInput.RtspUsername,
	}

	//validate camera object
	if err := camera.Validate(); err != nil {
		responseError(c, http.StatusBadRequest, err)
		return
	}

	//create camera
	createdCamera, err := collectorFound.AddCamera(server.DB, camera)
	if err != nil {
		responseError(c, http.StatusInternalServerError, err)
		return
	}
	c.JSON(http.StatusOK, createdCamera)
}

func (server *Server) readCameraHandler(c *gin.Context) {
	//parse camera id from params
	id, err := parseUintParam(c, "id")
	if err != nil {
		responseError(c, http.StatusBadRequest, err)
		return
	}

	//find camera
	var camera models.Camera
	cameraFound, err := camera.FindCameraByID(server.DB, id)
	if err != nil {
		responseError(c, http.StatusNotFound, err)
		return
	}
	c.JSON(http.StatusOK, cameraFound)
}

func (server *Server) updateCameraHandler(c *gin.Context) {
	//parse camera id from params
	id, err := parseUintParam(c, "id")
	if err != nil {
		responseError(c, http.StatusBadRequest, err)
		return
	}

	//check camera exists
	var camera models.Camera
	foundCamera, err := camera.FindCameraByID(server.DB, id)
	if err != nil {
		responseError(c, http.StatusNotFound, err)
		return
	}

	//build camera object from post data
	var cameraUpdate models.Camera
	if err := c.ShouldBindJSON(&cameraUpdate); err != nil {
		responseError(c, http.StatusBadRequest, err)
		return
	}
	cameraUpdate.CameraId = foundCamera.CameraId

	//validate camera object
	if err := cameraUpdate.Validate(); err != nil {
		responseError(c, http.StatusBadRequest, err)
		return
	}

	//update camera
	updatedCamera, err := cameraUpdate.UpdateCamera(server.DB)
	if err != nil {
		responseError(c, http.StatusInternalServerError, err)
		return
	}
	c.JSON(http.StatusOK, updatedCamera)
}

func (server *Server) deleteCameraHandler(c *gin.Context) {
	//parse camera id from params
	id, err := parseUintParam(c, "id")
	if err != nil {
		responseError(c, http.StatusBadRequest, err)
		return
	}

	//check camera exists
	var camera models.Camera
	foundCamera, err := camera.FindCameraByID(server.DB, id)
	if err != nil {
		responseError(c, http.StatusNotFound, err)
		return
	}

	//delete camera
	idDeleted, err := camera.DeleteCamera(server.DB, foundCamera.CameraId)
	if err != nil {
		responseError(c, http.StatusInternalServerError, err)
		return
	}
	c.JSON(http.StatusOK, gin.H{"deleted": idDeleted})
}
