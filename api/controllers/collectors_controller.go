package controllers

import (
	"cloudvms-database/api/models"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
)

func (server *Server) createCollectorHandler(c *gin.Context) {
	//parse inputs
	var collectorInput models.CollectorInput
	if err := c.ShouldBindJSON(&collectorInput); err != nil {
		responseError(c, http.StatusBadRequest, err)
		return
	}
	//check collector company exists
	var company models.Company
	companyFound, err := company.FindCompanyByID(server.DB, collectorInput.CompanyId)
	if err != nil {
		responseError(c, http.StatusBadRequest, fmt.Errorf("error on retreiving collector company : %s", err))
		return
	}

	//build collector object from inputs
	collector := &models.Collector{
		Name: collectorInput.Name,
	}

	//validate collector object
	if err := collector.Validate(); err != nil {
		responseError(c, http.StatusBadRequest, err)
		return
	}

	//add collector to company
	createdCollector, err := companyFound.AddCollector(server.DB, collector)
	if err != nil {
		responseError(c, http.StatusInternalServerError, err)
		return
	}

	c.JSON(http.StatusOK, createdCollector)
}
