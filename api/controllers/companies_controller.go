package controllers

import (
	"cloudvms-database/api/models"
	"net/http"

	"github.com/gin-gonic/gin"
)

func (server *Server) createCompanyHandler(c *gin.Context) {
	//parse inputs
	var companyInput models.CompanyInput
	if err := c.ShouldBindJSON(&companyInput); err != nil {
		responseError(c, http.StatusBadRequest, err)
		return
	}

	//build company object from inputs
	company := models.Company{
		Name: companyInput.Name,
	}

	//validate camera object
	if err := company.Validate(); err != nil {
		responseError(c, http.StatusBadRequest, err)
		return
	}

	//create company
	createdCompany, err := company.CreateCompany(server.DB)
	if err != nil {
		responseError(c, http.StatusInternalServerError, err)
		return
	}
	c.JSON(http.StatusOK, createdCompany)
}
