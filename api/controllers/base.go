package controllers

import (
	"cloudvms-database/api/models"
	"cloudvms-database/config"
	"fmt"
	"strconv"

	"github.com/gin-gonic/gin"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

type Server struct {
	DB     *gorm.DB
	router *gin.Engine
}

func (server *Server) Initialize(cfg *config.Database) (err error) {
	err = server.InitializeDatabase(cfg)
	if err != nil {
		return err
	}
	err = server.DB.AutoMigrate(&models.Camera{}, &models.Collector{}, &models.Company{}) //database migration
	if err != nil {
		return err
	}
	err = server.DB.Migrator().CreateConstraint(&models.Company{}, "Collector")
	if err != nil {
		return err
	}
	err = server.DB.Migrator().CreateConstraint(&models.Collector{}, "Camera")
	if err != nil {
		return err
	}
	server.router = gin.Default()
	server.initializeRoutes()
	return nil
}

func (server *Server) InitializeDatabase(cfg *config.Database) (err error) {
	dsn := fmt.Sprintf("host=%s port=%s user=%s dbname=%s sslmode=disable password=%s", cfg.Host, cfg.Port, cfg.User, cfg.Database, cfg.Password)
	server.DB, err = gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		return fmt.Errorf("cannot connect to postgres database\n error:%s", err)
	}
	return nil
}

func (server *Server) Run(addr string) {
	fmt.Printf("Listening to port %s\n", addr)
	server.router.Run()
}

func responseError(c *gin.Context, httpStatus int, err error) {
	c.JSON(httpStatus, gin.H{
		"error": err.Error(),
	})
}

func parseUintParam(c *gin.Context, param string) (uint64, error) {
	idStr, ok := c.Params.Get(param)
	if !ok {
		return 0, fmt.Errorf("unable de find %s param", param)
	}
	id, err := strconv.ParseUint(idStr, 10, 64)
	if err != nil {
		return 0, fmt.Errorf("unable to parse %s param : %s", param, err)
	}
	return id, nil
}

func parseUintParams(c *gin.Context, params []string) ([]uint64, error) {
	var values []uint64
	for _, param := range params {
		id, err := parseUintParam(c, param)
		if err != nil {
			return nil, err
		}
		values = append(values, id)
	}
	return values, nil
}
