package controllers

func (s *Server) initializeRoutes() {
	s.router.POST("/company/", s.createCompanyHandler)

	s.router.POST("/collector/", s.createCollectorHandler)
	s.router.POST("/collector/:collector_id/camera", s.createCollectorHandler)

	s.router.GET("/camera/:id", s.readCameraHandler)
	s.router.POST("/camera/", s.createCameraHandler)
	s.router.PATCH("/camera/:id", s.updateCameraHandler)
	s.router.DELETE("/camera/:id", s.deleteCameraHandler)
}
