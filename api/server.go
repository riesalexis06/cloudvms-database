package api

import (
	"cloudvms-database/api/controllers"
	"cloudvms-database/config"
	"log"
)

var server = controllers.Server{}

func Run() {
	config.LoadOptions()
	config.LoadConfigs()
	err := server.Initialize(config.DatabaseConfig)
	if err != nil {
		log.Fatal(err)
	}

	server.Run(":8080")
}
