module cloudvms-database

go 1.16

require (
	github.com/gin-gonic/gin v1.7.1
	github.com/go-pg/pg/v10 v10.9.1
	github.com/go-redis/redis v6.15.9+incompatible
	github.com/go-redis/redis/v8 v8.8.2
	github.com/gorilla/mux v1.8.0
	github.com/gorilla/websocket v1.4.2
	github.com/sirupsen/logrus v1.8.1
	github.com/spf13/pflag v1.0.5
	github.com/spf13/viper v1.7.1
	gorm.io/driver/mysql v1.0.6
	gorm.io/driver/postgres v1.0.8
	gorm.io/driver/sqlite v1.1.4
	gorm.io/gorm v1.21.9
)
